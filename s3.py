import json
import boto3

def lambda_handler(event, context):
    
    client= boto3.client('cloudwatch')
    S3_client = boto3.client('s3')
    
    Buckets = S3_client.list_buckets().get('Buckets')
    for i in Buckets:
        bucketname=i.get('Name')
        print(bucketname)
 
        client.put_metric_alarm(
            AlarmName='S3 '+str(bucketname)+' BucketSizeBytes Alarm',
            ComparisonOperator='GreaterThanOrEqualToThreshold',
            EvaluationPeriods=3,
            MetricName='BucketSizeBytes',
            Namespace='AWS/S3',
            Period=300,
            Statistic='Sum',
            Threshold=90,
            #AlarmActions= alarmaction,
            AlarmDescription='Alarm when Bucket Size Bytes exceeds X',
            Dimensions=[
                {
                'Name': 'BucketName','Value': str(bucketname)
                },
            ],
            TreatMissingData='notBreaching',
            Unit= 'Count'
        )
        
        client.put_metric_alarm(
            AlarmName='S3 '+str(bucketname)+' NumberOfObjects Alarm',
            ComparisonOperator='GreaterThanOrEqualToThreshold',
            EvaluationPeriods=3,
            MetricName='NumberOfObjects',
            Namespace='AWS/S3',
            Period=300,
            Statistic='Sum',
            Threshold=90,
            #AlarmActions= alarmaction,
            AlarmDescription='Alarm when Number Of Objects exceeds X',
            Dimensions=[
                {
                'Name': 'BucketName','Value': str(bucketname)
                },
            ],
            TreatMissingData='notBreaching',
            Unit= 'Count'
        )
    # TODO implement
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }

