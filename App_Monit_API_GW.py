import json
import boto3

def lambda_handler(event, context):
    
    client= boto3.client('cloudwatch')
    API_client = boto3.client('apigateway')
    
    
    x=API_client.get_rest_apis(
        limit=123
    ).get('items')
    for i in x:
        api_name=(i.get('name'))
        id=(i.get('id'))
        
        #creating alarms for different metrics
        client.put_metric_alarm(
            AlarmName='API_GW_'+str(api_name)+' 4XX Errors Alarm',
            ComparisonOperator=event["4xx_ComparisonOperator"],
            EvaluationPeriods=event["4xx_EvaluationPeriods"],
            MetricName='4XXError',
            Namespace='AWS/ApiGateway',
            Period=event["4xx_Period"],
            Statistic=event["4xx_Statistics"],
            Threshold=event["4xx_Threshold"],
            AlarmActions=[
                event["SNS_Topic"]
                ],
            AlarmDescription='Alarm when api gateway 4xx errors exceeds X',
            Dimensions=[
                {
                'Name': 'ApiName','Value': str(api_name)
                },
            ],
            Unit='Count'
        )
    
        client.put_metric_alarm(
            AlarmName='API_GW_'+str(api_name)+' _5XX Errors Alarm',
            ComparisonOperator=event["5xx_ComparisonOperator"],
            EvaluationPeriods=event["5xx_EvaluationPeriods"],
            MetricName='5XXError',
            Namespace='AWS/ApiGateway',
            Period=event["5xx_Period"],
            Statistic=event["5xx_Statistics"],
            Threshold=event["5xx_Threshold"],
            AlarmActions=[
            event["SNS_Topic"]
            ],
            AlarmDescription='Alarm when api gateway 5xx errors exceeds X',
            Dimensions=[
                {
                'Name': 'ApiName','Value': str(api_name)
                },
            ],
            Unit='Count'
        )
        
        client.put_metric_alarm(
            AlarmName='API_GW_'+str(api_name)+' Cache Miss Count Alarm',
            ComparisonOperator=event["Cache_Miss_Count_ComparisonOperator"],
            EvaluationPeriods=event["Cache_Miss_Count_EvaluationPeriods"],
            MetricName='CacheMissCount',
            Namespace='AWS/ApiGateway',
            Period=event["Cache_Miss_Count_Period"],
            Statistic=event["Cache_Miss_Count_Statistics"],
            Threshold=event["Cache_Miss_Count_Threshold"],
            AlarmActions=[
            event["SNS_Topic"]
            ],
            AlarmDescription='Alarm when api gateway Cache Miss Count errors exceeds X',
            Dimensions=[
                {
                    'Name': 'ApiName','Value': str(api_name)
                },
            ],
            Unit='Count'
        )
        
        client.put_metric_alarm(
            AlarmName='API_GW_'+str(api_name)+' Cache Hit Count Alarm',
            ComparisonOperator=event["Cache_Hit_Count_ComparisonOperator"],
            EvaluationPeriods=event["Cache_Hit_Count_EvaluationPeriods"],
            MetricName='CacheHitCount',
            Namespace='AWS/ApiGateway',
            Period=event["Cache_Hit_Count_Period"],
            Statistic=event["Cache_Hit_Count_Statistics"],
            Threshold=event["Cache_Hit_Count_Threshold"],
            AlarmActions=[
            event["SNS_Topic"]
            ],
            AlarmDescription='Alarm when api gateway Cache Hit Count exceeds X',
            Dimensions=[
                {
                'Name': 'ApiName','Value': str(api_name)
                },
            ],
            Unit='Count'
        )
        
        client.put_metric_alarm(
            AlarmName='API_GW_'+str(api_name)+' Integration Latency Alarm',
            ComparisonOperator=event["Integration_Latency_ComparisonOperator"],
            EvaluationPeriods=event["Integration_Latency_EvaluationPeriods"],
            MetricName='IntegrationLatency',
            Namespace='AWS/ApiGateway',
            Period=event["Integration_Latency_Period"],
            Statistic=event["Integration_Latency_Statistics"],
            Threshold=event["Integration_Latency_Threshold"],
            AlarmActions=[
            event["SNS_Topic"]
            ],
            AlarmDescription='Alarm when api gateway IntegrationLatency exceeds X',
            Dimensions=[
                {
                    'Name': 'ApiName','Value': str(api_name)
                },
            ],
            Unit='Milliseconds'
        )
        
        client.put_metric_alarm(
            AlarmName='API_GW_'+str(api_name)+' Latency Alarm',
            ComparisonOperator=event["Latency_ComparisonOperator"],
            EvaluationPeriods=event["Latency_EvaluationPeriods"],
            MetricName='Latency',
            Namespace='AWS/ApiGateway',
            Period=event["Latency_Period"],
            Statistic=event["Latency_Statistics"],
            Threshold=event["Latency_Threshold"],
            AlarmActions=[
            event["SNS_Topic"]
            ],
            AlarmDescription='Alarm when api gateway Latency Alarm exceeds X',
            Dimensions=[
                {
                'Name': 'ApiName','Value': str(api_name)
                },
            ],
            Unit='Milliseconds'
        )
        
        client.put_metric_alarm(
            AlarmName='API_GW_'+str(api_name)+' Count Alarm',
            ComparisonOperator=event["Count_ComparisonOperator"],
            EvaluationPeriods=event["Count_EvaluationPeriods"],
            MetricName='Count',
            Namespace='AWS/ApiGateway',
            Period=event["Count_Period"],
            Statistic=event["Count_Statistics"],
            Threshold=event["Count_Threshold"],
            AlarmActions=[
            event["SNS_Topic"]
            ],
            AlarmDescription='Alarm when api gateway Count exceeds X',
            Dimensions=[
                {
                'Name': 'ApiName','Value': str(api_name)
                },
            ],
            Unit='Count'
        )
        
        
        
        
        #start logging restapi
        p=API_client.get_stages(
            restApiId=str(id)
        ).get('item')
        for q in p:
            stagename=(q.get('stageName'))
        
      
        
        #start logging    
        API_client.update_stage(
            restApiId=str(id),
            stageName=str(stagename),
            patchOperations=[
                {
                    'op': 'replace',
                    'path': '/*/*/logging/loglevel',
                    'value': 'INFO',
                },
            ]
        )

    # TODO implement
    return {
        'body': json.dumps('Successfully created metric alarms and logging of API Gateways')
    }
        