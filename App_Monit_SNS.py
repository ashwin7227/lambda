import json
import boto3


def lambda_handler(event, context):
    
    client= boto3.client('cloudwatch')
    SNS_client = boto3.client('sns')
    IAM_client = boto3.client('iam')
    sts_client=boto3.client("sts")
    account_id=sts_client.get_caller_identity()["Account"]    
    
    #SNS Success policy 
    SNSSuccesspolicy={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"AWS": f"{account_id}"},
                "Action": [
                    "logs:CreateLogGroup",
                    "logs:CreateLogStream",
                    "logs:PutLogEvents",
                    "logs:PutMetricFilter",
                    "logs:PutRetentionPolicy"
                    ],
                "Resource": ["*"]    
            }
            ]
        }
    
    #SNS Failure policy
    SNSFailurepolicy={
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {"AWS": f"{account_id}"},
                "Action": [
                                        "logs:CreateLogGroup",
                    "logs:CreateLogStream",
                    "logs:PutLogEvents",
                    "logs:PutMetricFilter",
                    "logs:PutRetentionPolicy"
                    ],
                "Resource": ["*"]    
            }
            ]
    }

    #trust relationship policy
    trustpolicydoc={
    "Version": "2012-10-17",
    "Statement": {
        "Effect": "Allow",
        "Principal": {"Service": "firehose.amazonaws.com"},
        "Action": "sts:AssumeRole"
        }
    }
    
    trustpolicydoc=json.dumps(trustpolicydoc)
    SNSSuccesspolicy=json.dumps(SNSSuccesspolicy)
    SNSFailurepolicy=json.dumps(SNSFailurepolicy)

    #check if policy exist or create it
    try:
        cond=IAM_client.get_role(
        RoleName='SNSSuccessFeedback'
        )
        print("SNSSuccessFeedback Role is already available")
    except:
        IAM_client.create_role(
            RoleName='SNSSuccessFeedback',
            AssumeRolePolicyDocument=trustpolicydoc
        )
    
        IAM_client.put_role_policy(
            RoleName='SNSSuccessFeedback',
            PolicyName='click',
            PolicyDocument=SNSSuccesspolicy
        )
    
    try:
        cond=IAM_client.get_role(
        RoleName='SNSFailureFeedback'
        )
        print("SNSFailureFeedback Role is already available")
    except:
        IAM_client.create_role(
            RoleName='SNSFailureFeedback',
            AssumeRolePolicyDocument=trustpolicydoc
        )
    
        IAM_client.put_role_policy(
            RoleName='SNSFailureFeedback',
            PolicyName='click1',
            PolicyDocument=SNSFailurepolicy
        )

    SNS_client.set_platform_application_attributes(
        PlatformApplicationArn=f'arn:aws:sns:us-east-2:{account_id}:dynamodb',
        Attributes={
            'SuccessFeedbackSampleRate': '100'
            
        }
    )
    
    """print(SNS_client.get_topic_attributes(
        TopicArn=f'arn:aws:sns:us-east-2:{account_id}:new_posts'
    )
    )"""
    
    response = SNS_client.list_topics().get('Topics')
    for i in response:
        topicarn=(i.get('TopicArn'))
        client.put_metric_alarm(
            AlarmName=str(topicarn)+' Notifications Delivered Alarm',
            ComparisonOperator='GreaterThanOrEqualToThreshold',
            EvaluationPeriods=3,
            MetricName='NumberOfNotificationsDelivered',
            Namespace='AWS/SNS',
            Period=300,
            Statistic='Sum',
            Threshold=50,
            #AlarmActions= alarmaction,
            AlarmDescription='Alarm when Number Of Notifications Delivered in SNS exceeds X',
            Dimensions=[
                {
                'Name': 'TopicArn','Value': str(topicarn)
                },
            ],
            TreatMissingData='notBreaching',
            Unit= 'Count'
        )
        
        client.put_metric_alarm(
            AlarmName=str(topicarn)+' Notifications Failed Alarm',
            ComparisonOperator='GreaterThanOrEqualToThreshold',
            EvaluationPeriods=3,
            MetricName='NumberOfNotificationsFailed',
            Namespace='AWS/SNS',
            Period=300,
            Statistic='Sum',
            Threshold=50,
            #AlarmActions= alarmaction,
            AlarmDescription='Alarm when Number Of Notifications Failed in SNS exceeds X',
            Dimensions=[
                {
                'Name': 'TopicArn','Value': str(topicarn)
                },
            ],
            TreatMissingData='notBreaching',
            Unit= 'Count'
        )
        
        
    # TODO implement
    return {
        'body': json.dumps('Successfully Cloud Watch Alarms are created for SNS')
    }

