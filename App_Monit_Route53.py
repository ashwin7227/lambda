import json
import boto3


def lambda_handler(event, context):
    client=boto3.client('cloudwatch')
    CWLogs_client=boto3.client('logs', region_name='us-east-1')
    R53_client=boto3.client('route53')
    sts_client=boto3.client("sts")
    
    #fetch account id
    account_id=sts_client.get_caller_identity()["Account"]
    
    HealthChecks = R53_client.list_health_checks().get('HealthChecks')
    for i in HealthChecks:
        health_check_id=(i.get('Id'))
        
        client.put_metric_alarm(
            AlarmName=str(health_check_id)+'DNS Queries Monitoring in Route53',
            ComparisonOperator='GreaterThanOrEqualToThreshold',
            EvaluationPeriods=3,
            MetricName='DNSQueries',
            Namespace='AWS/Route53',
            Period=300,
            Statistic='Sum',
            Threshold=50,
            #AlarmActions= alarmaction,
            AlarmDescription='Alarm when DNS Queries Monitoring in Route53 exceeds X',
            Dimensions=[
                {
                'Name': 'Name','Value': str(health_check_id)
                },
            ],
            TreatMissingData='notBreaching',
            Unit= 'Count'
        )
        
        client.put_metric_alarm(
            AlarmName=str(health_check_id)+'HealthCheckPercentageHealthy',
            ComparisonOperator='GreaterThanOrEqualToThreshold',
            EvaluationPeriods=3,
            MetricName='HealthCheckPercentageHealthy',
            Namespace='AWS/Route53',
            Period=300,
            Statistic='Average',
            Threshold=50,
            #AlarmActions= alarmaction,
            AlarmDescription='Alarm when HealthCheck Percentage Healthy Monitoring in Route53 exceeds X',
            Dimensions=[
                {
                'Name': 'Name','Value': str(health_check_id)
                },
            ],
            TreatMissingData='notBreaching',
            Unit= 'Percent'
        )
        
        client.put_metric_alarm(
            AlarmName=str(health_check_id)+'HealthCheck Status',
            ComparisonOperator='GreaterThanOrEqualToThreshold',
            EvaluationPeriods=3,
            MetricName='HealthCheckStatus',
            Namespace='AWS/Route53',
            Period=300,
            Statistic='Minimum',
            Threshold=50,
            #AlarmActions= alarmaction,
            AlarmDescription='Alarm when HealthCheck Status Monitoring in Route53 exceeds X',
            Dimensions=[
                {
                'Name': 'Name','Value': str(health_check_id)
                },
            ],
            TreatMissingData='notBreaching'
        )
    
    #Resource policy document
    policy={
        "Version":"2012-10-17",
        "Statement":[
            {
                "Effect":"Allow",
                "Principal": {
                    "Service": "route53.amazonaws.com"
                },
                "Action":[
                    "logs:CreateLogStream",
                    "logs:PutLogEvents"
                    ],
                    "Resource":f"arn:aws:logs:us-east-1:{account_id}:log-group:/aws/route53/*"
            }
            ]
    }
    policy = json.dumps(policy)
    #create log resource policy
    response = CWLogs_client.put_resource_policy(
        policyName='route53-query-logging-policy',
        policyDocument= policy
    )
    
    hosted_zones = R53_client.list_hosted_zones().get('HostedZones')
    for j in hosted_zones:
        x=(j.get('Name'))
        domain=x[:-1]
        
        # create log group for DNS query loggging
        response = CWLogs_client.create_log_group(
            logGroupName='/aws/route53/'+str(domain)
        )
        
        
    # TODO implement
    return {
        'body': json.dumps('Successfully created Cloud Watch alarms for Route53')
    }
    
